﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteIsaac.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Portfolio()
        {

            return View();
        }

        public ActionResult BOMComparisonTool()
        {

            return View("_BOMComparisonTool");
        }

        public ActionResult Calculator()
        {

            return View("_Calculator");
        }

        public ActionResult CookBook()
        {

            return View("_CookBook");
        }

        public ActionResult EncodeThis()
        {

            return View("_EncodeThis");
        }

        public ActionResult MOBNation()
        {

            return View("_MOBNation");
        }

        public ActionResult SPSToolBox()
        {

            return View("_SPSToolBox");
        }

        public ActionResult SimpleAsRun()
        {

            return View("_SimpleAsRun");
        }

    }
}